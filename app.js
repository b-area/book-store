const express = require('express');
const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID;

const app = express();
const PORT = 5050
const DB_URI = 'mongodb://localhost:27017';

const DB_NAME = 'book_store';
const BOOK_COLLECTION_NAME = "Books";

const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

app.use(bodyParser.urlencoded({ extended: false })) // allow user to send data within the URl
app.use(bodyParser.json()); // allow user to send JSON dat

// Retrieving all records
// GET endpoint
app.get('/books/', function(req, res) {

  client.connect(function(err, connection){
    const db = connection.db(DB_NAME); // connecting to the book store database
    db.collection(BOOK_COLLECTION_NAME)
      .find({})
      .toArray(function(find_err, records){
        if (find_err)
          return res.status(500).send(find_err);

        return res.send(records);
      });

  });
})


// Creating new records
app.post('/books/', function(req, res){

  if (!req.body || req.body.length === 0)
    return res.status(400).send({"message": "record is requried"});

  // data validation
  if (!req.body.title || !req.body.author || !req.body.price)
    res.status(400).send({
      message: "Title, author, price, ... are requried"
    });

  // date is in req.body
  client.connect(function(err, connection){
    const db = connection.db(DB_NAME);

    db.collection(BOOK_COLLECTION_NAME)
      .insertOne(req.body, function(insert_error, data){
        if (insert_error)
          return res.status(500).send({message: "Something went wrong"});

        connection.close();

        return res.status(200).send({message: "Reocrd inserted successfully"});
      });

  });


})

app.put('/books/:id', function(req, res) {

  client.connect(function(err, connection) {
    if (err)
      return res.status(500).send({error: err});

    const db = connection.db(DB_NAME);
    db.collection(BOOK_COLLECTION_NAME)
      .updateOne({_id: ObjectID(req.params.id)},{$set: req.body}, function(update_err, update_data){
         if (update_err)
          return res.status(500).send({error: update_err, message: "Could not update the record"});

        return res.status(200).send({message: "Update was successful!", data: update_data});
      });

  });

})

app.listen(PORT);
console.log("Listening on port " + PORT);
